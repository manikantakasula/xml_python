from dicttoxml import dicttoxml
Employee = {
  "emp1" : {
    "emp_name" : "abc",
    "emp_designation": "Software Engineer",
    "emp_id" : 90
  },
  "emp2" : {
    "emp_name" : "def",
    "emp_designation": "doctor",
    "emp_id" : 91
  },
  "emp3" : {
    "emp_name" : "ghi",
    "emp_designation": "manager",
    "emp_id" : 92
  }
} 

xml=dicttoxml(Employee)
xmlfile = open("Employees.xml", "w")
xmlfile.write(xml.decode())
xmlfile.close()

