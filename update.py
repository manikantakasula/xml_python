from base64 import encode
import xml.etree.ElementTree as ET
tree = ET.parse('example.xml')
root = tree.getroot()

#for updating existing field
for rank in root.iter('rank'):
	new_rank = int(rank.text) + 1
	rank.text = str(new_rank)
	rank.set('updated', 'yes')
tree.write('output1.xml')

#Insert new field into <fields>
data = ET.tostring(root,encoding = "utf8",method = "xml")
root = ET.fromstring(data)
new_field = ET.Element("Country",name="India")
ET.SubElement(new_field, 'rank').text = "1"
ET.SubElement(new_field, 'year').text = "1947"
ET.SubElement(new_field, 'gdppc').text = "2500"
ET.SubElement(new_field, 'neighbour').text = "sri lanka"
root.insert(3, new_field)

ET.dump(root)
tree = ET.ElementTree(root)
tree.write(open('test.xml','w'), encoding='unicode')

