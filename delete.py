import xml.etree.ElementTree as ET
tree = ET.parse('example.xml')
root = tree.getroot()

for country in root.findall('country'):
	# using root.findall() to avoid removal during traversal
	rank = int(country.find('rank').text)
	if rank > 50:
		root.remove(country)

tree.write('deleted_output.xml')
